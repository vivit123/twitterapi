using System;
using System.Collections.Generic;
using System.Linq;

namespace TwitterApi.Models
{
    public class FollowingRepository : IFollowingRepository
    {
        private readonly TwitterContext _context;
        public FollowingRepository(TwitterContext context)
        {
            _context = context;
        }
        public void Add(int userid, User following)
        {
            Following fl = new Following();
            User u = _context.Users.Find(userid);
            if(u == null) {
                return;
            }
            fl.User = u.UserID;
            fl.Follow = -1;
            if(following.UserID != -1) {
                foreach (User user in _context.Users)
                {
                    if (user.UserID == following.UserID)
                    {
                        fl.Follow = user.UserID;
                    }
                }
            } else if(following.Name != "") {
                foreach (User user in _context.Users)
                {
                    if (user.Name == following.Name)
                    {
                        fl.Follow = user.UserID;
                    }
                }
            }
            _context.Followings.Add(fl);
            _context.SaveChanges();
        }

        public Following Find(int key)
        {
            return _context.Followings.Find(key);
        }

        public IEnumerable<Following> GetAll()
        {
            return _context.Followings;
        }

        public IEnumerable<Following> GetAllByUser(string userid)
        {
            int id = Int32.Parse(userid);
            return _context.Followings
                    .Where(b => b.User == id)
                    .ToList();
        }

        public Following Remove(int key)
        {
            Following following = Find(key);
            _context.Followings.Remove(following);
            _context.SaveChanges();
            return following;
        }

        public void Update(Following following)
        {
            _context.Followings.Update(following);
            _context.SaveChanges();
        }
    }
}